# MATHCASTER
This is a simple 2D raycaster written for the [Love2d](https://love2d.org/) framework.
It was developed for a final exam so it's quite simple and useless outside of
demonstrating vector/line intersection.

### Controls
You start in the centre of a 2x2 box. use the WASD keys to move and the mouse to look
as in typical first person games. You cannot look up and down since this is a 2D
engine.

### How to run
After cloning the repository, make sure to run `git submodule update --init --recursive`
since the project relies on the [hump](https://github.com/vrld/hump) helper utilities.

You will need [Love2d](https://love2d.org) on your system. either run `love .` from
from the project directory, or zip up the files and name it `mathcaster.love` or
something similar. In Macintosh and Windows, you should be able to double click this
file and Love2d will run it as long as it can find `main.lua` in the top of the zip
file.

### License
I'm not sure why you would want to use this code for anything. It's published under
the GPL 3.0 because I don't expect it to be incorperated into anything but I want you
to be able to see the source.